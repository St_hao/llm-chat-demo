## <font color=blue > 前言介绍🌈</font>

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <font color='#956FE7' size=4>[Llm-Chat-Demo🔍](https://gitee.com/St_hao/llm-chat-demo)</font>是一个简单的自定义的**大模型语言交互界面**，目前后端使用**通义千问的网络API**提供数据，前端调用接口实现浏览器页面的流式输出。Llm-Chat-Demo旨在以简单、优雅的练习大模型语言交互界面。

---

![输入图片说明](chat-frontend/public/img/661bc8f3f3df4c60b767a641dd87f262.png)

---

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;本项目的gitee代码地址为：[https://gitee.com/St_hao/llm-chat-demo.git](https://gitee.com/St_hao/llm-chat-demo.git)。




![输入图片说明](chat-frontend/public/img/5f67afdd6b9241edbef253dd7e589a6b.png)

&nbsp;

---


@[toc]

---
### <font color='#956FE7'>项目基础🔗</font>

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[<font color=LightPink>Vue2实现流式输出–新手教程）</font>](https://blog.csdn.net/weixin_51649510/article/details/138328421?spm=1001.2014.3001.5502) --Vue基础

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;该项目的**后端代码的编写**以及下面三个**基础教学**由 [浩浩的科研笔记](https://chen-hao.blog.csdn.net/?type=blog) 提供。

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[<font color=Feldspar>Flask框架初探-如何在本机发布一个web服务并通过requests访问自己发布的服务-简易入门版（Flask基础）</font>](https://chen-hao.blog.csdn.net/article/details/137874720) --（Flask基础）

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[<font color=Feldspar>Flask框架进阶-Flask流式输出和受访配置--纯净详解版</font>](https://chen-hao.blog.csdn.net/article/details/138276641?spm=1001.2014.3001.5502) --（Flask基础）

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[调用阿里通义千问大语言模型API-小白新手教程-python](https://chen-hao.blog.csdn.net/article/details/135868918?spm=1001.2014.3001.5502) --（大模型基础）


---

## <font color=DarkSalmon>技术架构</font>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**简介**：一款可以实现多轮对话，流式输出的大模型语言交互界面。
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**系统架构**：前端`Vue` + 后端`Flask`。
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**输出形式**：	流式输出。

---

### <font color=DarkSalmon>前端启动流程</font>

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;前端使用Vue2并采用SSE技术实现流式输出，启动项目你需在前端的工程目录下执行以下命令。按照前端工程放在`D:\llm-chat-demo\chat-frontend`目录为例。`Win+R`，输入`cmd`，按下回车进入控制台。





```bash
C:\Users\Administrator>D: #进入D盘

D:\>cd D:\llm-chat-demo\chat-frontend #进入到前端的工程目录

D:\llm-chat-demo\flask_backend>npm install #安装依赖

D:\llm-chat-demo\flask_backend>npm run serve #启动命令
```

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;前端代码启动后效果
![输入图片说明](chat-frontend/public/img/fff2e69266e24168957f77d65f661e03.png)



---
### <font color=DarkSalmon>后端启动流程</font>


&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;后端代码运行`appllm.py` 文件，开启5000端口，便可以调用通义千问大语言模型API实现智能回答。
按照后端工程放在`D:\llm-chat-demo\flask_backend`目录为例。`Win+R`，输入`cmd`，按下回车进入控制台。
```python
C:\Users\Administrator>D: #进入D盘

D:\>cd D:\llm-chat-demo\flask_backend #进入到后端的工程目录

D:\llm-chat-demo\flask_backend>pip install flask #使用pip命令安装flask。

D:\llm-chat-demo\flask_backend>python appllm.py #启动程序
```

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;后端代码启动后效果
![输入图片说明](chat-frontend/public/img/16cc0d3b1b284c16baf9432f4cd1b43e.png)






---

### <font color=DarkSalmon>项目演示效果</font>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;网站的请求地址为`http://localhost:8080/#/`  。 在输入框中输入想要询问的问题，就可以得到通义千问大语言模型给出的智能回答。
![输入图片说明](chat-frontend/public/img/be046ca0e14249b4ab022f1758ac357a.gif)


---



## 总结
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;本项目主要遇到的难点有两条。
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;第一条：是对于后端返回的`markdown`格式的数据，前端如何进行格式解析并且展示出来,最终是利用`markdown-it`和`highlight.js`依赖库进行配合，才最终得以正确展示。
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;第二条：是对于`SSE (Server-Sent Events)`技术的学习和了解。才能最终实现页面流式输出的效果。
